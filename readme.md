Form Validation
===============

We are using v0.6.2 which needs jQuery, the latest versions might not need jquery as they removed jQuery as their dependency from v1.0.0 and implemented Form Validation using Typescript and ES6 from scratch…

Before v0.6, it was known as Bootstrap Validator

Link to v0.6 doc: [https://old.formvalidation.io/getting-started/](https://old.formvalidation.io/getting-started/)

Setup
=====

You need to add below styles and scripts in your page

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/formvalidation/0.6.2-dev/css/formValidation.min.css" integrity="sha512-B9GRVQaYJ7aMZO3WC2UvS9xds1D+gWQoNiXiZYRlqIVszL073pHXi0pxWxVycBk0fnacKIE3UHuWfSeETDCe7w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/formvalidation/0.6.2-dev/js/formValidation.min.js" integrity="sha512-DlXWqMPKer3hZZMFub5hMTfj9aMQTNDrf0P21WESBefJSwvJguz97HB007VuOEecCApSMf5SY7A7LkQwfGyVfg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/formvalidation/0.6.2-dev/js/framework/bootstrap.min.js" integrity="sha512-CwWsnPwntKMVNsVVCKIxPd4Ievk/YyAxt/yFNOLbs4JH3W6djpxYf2G50DtxLxFGHIbZxXeVDyjmTT8RCNp8DQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    

Form will look like this:

    <form action="#" id="testForm" method="POST">
    	<div class="card-body">
            <div class="form-group mb-3">
                <label for="user-name" class="form-label">Name</label>
                <input type="text" id="user-name" name="userName" class="form-control" placeholder="Please enter your name">
            </div>
    
            <div class="form-group">
                <label for="user-email" class="form-label">Email</label>
                <input type="text" id="user-email" name="userEmail" class="form-control" placeholder="Please enter your Email">
            </div>
            <div class="form-group mb-3">
                <label for="user-email" class="form-label">Email</label>
                <input type="email" id="user-email" name="userEmail" class="form-control" placeholder="Please enter your Email">
            </div>
    
            <div class="form-group mb-3">
                <label for="user-phone" class="form-label">Phone</label>
                <input type="text" id="user-phone" name="userPhone" class="form-control" placeholder="Please enter your Phone Number">
            </div>
    
            <div class="form-group mb-3">
                <label for="user-date" class="form-label">Date</label>
                <input id="user-date" name="userDate" type="date" class="form-control flatpickr-date" placeholder="Select Date">
            </div>
    
            <div class="form-group mb-3">
                <label for="user-number" class="form-label">Enter Number</label>
                <input id="user-number" name="userNumber" type="number" class="form-control" placeholder="Enter a number">
            </div>
    
            <div class="form-group">
                <label for="user-file" class="form-label">Select File</label>
                <input type="file" id="user-file" name="userExcel" class="form-control">
            </div>
      </div>
      <div class="card-footer">
          <input id="submit" type="submit" value="Submit" class="btn btn-primary">
      </div>
    </form>
Script will be like:

    // Initialization of Form Validator is same as of jQuery Validator
    // just we have to use formValidation() instead of validate();
    
    $(document).ready(function() {
        $("#testForm").formValidation({
            // framework: String [Optional] 
            // as we are using bootstrap framework, we will specify here 
            framework: 'bootstrap',
    
            // err: Object 
            // we can specify the style class that will be applied to the elements containing error message if there are any...
            // the main reason to specify this setting is to give style to the error message, as in the issues faced [added at the end of the page...] the error messages are printed like normal text
            err: {
                clazz: 'text-danger'
            },
    
            live: 'enabled', // By default it is true, specify whether to validate the form live or to validate it after being submitted...
            // values : enabled, disabled, submitted
            // NOTE: the live field is for the whole form and cannot apply for specific element
    
            // icons: Object, to specify the icon styles that will be displayed if the input has errors or is invalid.
            // options: valid, invalid, validating.
            icon: {
                valid: 'fa fa-check',
                invalid: 'fa fa-times',
                validating: 'fa fa-refresh'
            },
    
    
            // fields: Object, here we can specify the rules of validation, we need to specify name of the input tag inorder to register validation for that field
            fields: {
                userName: {
                    validators: {
                        notEmpty:{
                            message: 'The name field is required'
                        }, 
                        // for some special cases like alphaNumeric we need to specify regex equation and to do that we will use regexp
                        regexp: {
                            regexp: /^[a-zA-Z0-9_]+$/,
                            message: 'The name can only consist of alphabetical, number and underscore'
                        }
                    }
                },
                userEmail: {
                    validators: {
                        notEmpty: {
                            message: "The email field is requierd!"
                        },
                        // for validating email
                        emailAddress: {
                            message: "Please enter a valid email"
                        }
                    }
                },
                userDate: {
                    validators: {
                        notEmpty: {
                            message: "The date field is required"
                        },
                        // for validating date format
                        date: {
                            format: "YYYY-MM-DD",
                            message: "Please enter the date in YYYY-MM-DD format!"
                        }
                    }
                },
                userPhone: {
                    validators: {
                        notEmpty: {
                            message: 'The phone number field is required!'
                        },
                        // for validating phone number
                        phone: {
                            country: 'IN',
                            message: 'Please enter a valid phone number'
                        }
                    }
                }, 
                userNumber: {
                    validators: {
                        notEmpty: {
                            message: 'The number field is required!'
                        },
                        // to validate that the input contains only digits
                        digits: {
                            message: "Please enter digits only"
                        }
                    }
                },
                userExcel: {
                    validators: {
                        notEmpty: {
                            message: "Please select a file"
                        },
                        // for validating type of file
                        file: {
                            // specifying extension of file, multiple type of files can be added comma separated like this 
                            extension: 'xlsx,csv',
                            message: 'Please select a file of type Excel'
                        }
                    }
                }
            },
        });
    });

To know more validation rules provided in v0.6: [Form Validator v0.6](https://old.formvalidation.io/validators/)

[Container Options](https://old.formvalidation.io/examples/showing-message-custom-area/#using-container-option)

Issues faced
============

The main issue I got stuck in was because of designing the form…

You need to group input for one detail else it will exhaust the call stack as I was doing here

    <form action="#" id="testForm" method="POST">
        <div class="mb-3">
            <label for="user-name" class="form-label">Name</label>
            <input type="text" id="user-name" name="userName" class="form-control" placeholder="Please enter your name">
        </div>
    
        <div class="mb-3">
            <label for="user-email" class="form-label">Email</label>
            <input type="text" id="user-email" name="userEmail" class="form-control" placeholder="Please enter your Email">
        </div>
    
        <input type="submit" value="Submit" class="btn btn-primary">
    </form>

Then the error was

[![](src/images/Untitled.png)](src/images/Untitled.png)

So, if this happens make sure you have grouped each and every input like this:

    <form action="#" id="testForm" method="POST">
                        <div class="form-group">
                            <label for="user-name" class="form-label">Name</label>
                            <input type="text" id="user-name" name="userName" class="form-control" placeholder="Please enter your name">
                        </div>
    
                        <div class="form-group">
                            <label for="user-email" class="form-label">Email</label>
                            <input type="text" id="user-email" name="userEmail" class="form-control" placeholder="Please enter your Email">
                        </div>
    
                        <input type="submit" value="Submit" class="btn btn-primary">
                    </form>

Then on submitting blank form it works properly:

[![](src/images/Untitled%201.png)](src/images/Untitled%201.png)

NOTE: PLEASE GROUP UP EACH INPUT FIELD OR ELSE IF YOU TRY BEING LAZY LIKE HERE:

    <form action="#" id="testForm" method="POST">
        <div class="form-group">
            <label for="user-name" class="form-label">Name</label>
            <input type="text" id="user-name" name="userName" class="form-control" placeholder="Please enter your name">
            <label for="user-email" class="form-label">Email</label>
            <input type="text" id="user-email" name="userEmail" class="form-control" placeholder="Please enter your Email">
        </div>
        <input type="submit" value="Submit" class="btn btn-primary">
    </form>

Grouping more than one fields will give output like this

[![](src/images/Untitled%202.png)](src/images/Untitled%202.png)