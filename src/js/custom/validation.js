$(document).ready(function() {
    // Form Validation
    $("#testForm").formValidation({
        framework: 'bootstrap',
        err: {
            clazz: 'text-danger'
        },
        // live: 'enabled',
        icon: {
            valid: 'fa fa-check',
            invalid: 'fa fa-times',
            validating: 'fa fa-refresh'
        },
        fields: {
            userName: {
                validators: {
                    notEmpty:{
                        message: 'The name field is required'
                    }, 
                    // for some special cases like alphaNumeric we need to specify regex equation and to do that we will use regexp
                    regexp: {
                        regexp: /^[a-zA-Z0-9_]+$/,
                        message: 'The name can only consist of alphabetical, number and underscore'
                    }
                }
            },
            userEmail: {
                validators: {
                    notEmpty: {
                        message: "The email field is requierd!"
                    },
                    // for validating email
                    emailAddress: {
                        message: "Please enter a valid email"
                    }
                }
            },
            userDate: {
                validators: {
                    notEmpty: {
                        message: "The date field is required"
                    },
                    // for validating date format
                    date: {
                        format: "YYYY-MM-DD",
                        message: "Please enter the date in YYYY-MM-DD format!"
                    }
                }
            },
            userPhone: {
                validators: {
                    notEmpty: {
                        message: 'The phone number field is required!'
                    },
                    // for validating phone number
                    phone: {
                        country: 'IN',
                        message: 'Please enter a valid phone number'
                    }
                }
            }, 
            userNumber: {
                validators: {
                    notEmpty: {
                        message: 'The number field is required!'
                    },
                    // to validate that the input contains only digits
                    digits: {
                        message: "Please enter digits only"
                    }
                }
            },
            userExcel: {
                validators: {
                    notEmpty: {
                        message: "Please select a file"
                    },
                    // for validating type of file
                    file: {
                        // specifying extension of file, multiple type of files can be added comma separated like this 
                        extension: 'xlsx,csv',
                        message: 'Please select a file of type Excel'
                    }
                }
            }
        },
    });
});